﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoUp
{
    public partial class SignInForm : Form
    {
        LaDa lada = new LaDa("users");
        public SignInForm()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignUp frm = new SignUp();
            //CreateGroup frm2 = new CreateGroup();
            frm.Show();
        }

        private void SignInForm_Load(object sender, EventArgs e)
        {
            //MySql.Data.MySqlClient.MySqlConnection conn;
            //string myConnectionString;

            //myConnectionString = "server=127.0.0.1;uid=grynsztein;" +
            //    "pwd=3059356444;database=DoUp;";

            //try
            //{
            //    conn = new MySql.Data.MySqlClient.MySqlConnection();
            //    conn.ConnectionString = myConnectionString;
            //    conn.Open();
            //}
            //catch (MySql.Data.MySqlClient.MySqlException ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            /*
            AddMember frm3 = new AddMember();
            frm3.Show();
            */
            string username = usernameTextBox.Text;
            string password = passwordTextBox.Text;

            if (lada.existsTable("users"))
            {
                
                bool userExists = lada.existsValue("username", username);

                if (userExists)
                {
                    string correctPass = lada.getValue("username", username, "password");
                    if (correctPass == password)
                    {
                        MessageBox.Show("Login is correct!");
                    }
                    else
                    {
                        MessageBox.Show("Username or password is incorrect.");
                    }
                }
                else
                {
                    MessageBox.Show("Username or password is incorrect.");
                }
            }
            else
            {
                List<string> headers = new List<string>();
                headers.Add("username");
                headers.Add("password");
                lada.createTable(headers);
                MessageBox.Show("No users found!");
            }
            
        }

        private void createGroupButton_Click(object sender, EventArgs e)
        {
            CreateGroup crg = new CreateGroup();
            crg.Show();
        }

        private void addMemberButton_Click(object sender, EventArgs e)
        {
            AddMember adm = new AddMember();
            adm.Show();
        }
    }
}
