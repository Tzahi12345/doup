﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoUp
{
    class Group
    {
        Toolbox box = new Toolbox();

        // instance varialbes
        public string groupName;
        public List<string> folderList;
        public List<string> fieldList;
        public List<string> fieldTypeList;

        // constructor for Group object
        public Group(string name)
        {
            groupName = name;
        }

        // creates database file for group
        public void createGroup(List<string> folderList, List<string> fieldList, List<string> fieldTypeList)
        {
            string defaultDirectory = box.getDefaultDirectory();
            // defines file path for the group
            string groupFileName = System.IO.Path.Combine(
                defaultDirectory, groupName + "_group.txt");
            // defines file path for settings of group
            string settingsFileName = System.IO.Path.Combine(
                defaultDirectory, groupName + "_group_settings.txt");
            //string firstLine = "";
            //if (fieldList.Count == 1)
            //{
            //    firstLine = fieldList[0];
            //}
            //string secondLine = folderList[0];
            //// defines first line of the text file
            //for (int i = 1; i < fieldList.Count; i++)
            //{
            //    firstLine += "~" + fieldList[i];
            //}
            //// defines second line of the text file
            //for (int i = 1; i < folderList.Count; i++)
            //{
            //    secondLine += "~" + folderList[i];
            //}
            //// checks if file exists
            if (File.Exists(groupFileName) || File.Exists(settingsFileName))
            {
                //return error
            }
            else
            {
                ////writes lines to file
                //using (StreamWriter sw = File.CreateText(groupFileName))
                //{
                //    sw.WriteLine(firstLine);
                //    sw.WriteLine(secondLine);
                //    sw.WriteLine("");
                //}

                LaDa groupTable = new LaDa(groupName + "_group");

                LaDa settingsTable = new LaDa(groupName + "_group_settings");

                // converts folder list into a string
                string folderListString = "";
                if (folderList.Count > 0)
                {
                    folderListString = folderList[0];
                }
                for (int i = 1; i < folderList.Count; i++)
                {
                    folderListString += "," + folderList[i];
                }

                // adds the folders field to the start
                fieldList.Insert(0,"Folders");
                fieldList.Insert(0, "File");
                fieldTypeList.Insert(0,folderListString);

                groupTable.createTable(fieldList);
                settingsTable.createTable(fieldList);
                settingsTable.addRow(fieldTypeList);
            }
        }

        // adds new member to the group, basically another row to the database
        public void addMember(string fileName, string folder, List<string> fields)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string groupFileName = System.IO.Path.Combine(
                defaultDirectory, groupName + "_group.txt");
            fields.Insert(0, folder);
            fields.Insert(0, fileName);
            if (!(File.Exists(groupFileName)))
            {
                MessageBox.Show("Cannot add member as file does not exist!");
            }
            else
            {
                LaDa theGroup = new LaDa(groupName + "_group");
                theGroup.addRow(fields);
            }
        }
    }
}
