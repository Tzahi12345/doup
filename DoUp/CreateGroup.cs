﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoUp
{
    public partial class CreateGroup : Form
    {
        //LaDa lada = new LaDa();
        Toolbox box = new Toolbox();
        public CreateGroup()
        {
            InitializeComponent();
        }

        private void createGroupButton_Click(object sender, EventArgs e)
        {
            // get group name
            String groupName = groupNameTextBox.Text;
            if (groupName == "")
            {
                // throw error
            }

            // get folders
            List<string> theFolder = new List<string>();
            Array foldersArr;
            if (foldersTextBox.Text == "")
            {
                foldersArr = new string[1]{"Default"};
            }
            else
            {
                foldersArr = foldersTextBox.Text.Split(',');
                theFolder = foldersArr.OfType<string>().ToList();
            }



            // get fields and types
            List<string> fieldTypes = new List<string>();
            List<string> theField = new List<string>();
            if (field1TextBox.Text == "")
            {
                // throw error
            }
            if (field1TextBox.Text != "")
            {
                theField.Add(field1TextBox.Text);
                if (field1NumRadio.Checked)
                {
                    fieldTypes.Add("Num");
                }
                else
                {
                    fieldTypes.Add("Text");
                }
            }
            if (field2TextBox.Text != "")
            {
                theField.Add(field2TextBox.Text);
                if (field2NumRadio.Checked)
                {
                    fieldTypes.Add("Num");
                }
                else
                {
                    fieldTypes.Add("Text");
                }
            }
            if (field3TextBox.Text != "")
            {
                theField.Add(field3TextBox.Text);
                if (field3NumRadio.Checked)
                {
                    fieldTypes.Add("Num");
                }
                else
                {
                    fieldTypes.Add("Text");
                }
            }
            if (field4TextBox.Text != "")
            {
                theField.Add(field4TextBox.Text);
                if (field4NumRadio.Checked)
                {
                    fieldTypes.Add("Num");
                }
                else
                {
                    fieldTypes.Add("Text");
                }
            }
            Group newGroup = new Group(groupName);
            newGroup.createGroup(theFolder, theField, fieldTypes);
            this.Close();
        }

        private void CreateGroup_Load(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(box.getDefaultDirectory()))
            {
                // this is ok
            }
            else
            {
                Directory.CreateDirectory(box.getDefaultDirectory());
            }
        }
    }
}
