﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DoUp
{
    public partial class AddMember : Form
    {
        //LaDa lada = new LaDa();
        Toolbox box = new Toolbox();
        public AddMember()
        {
            InitializeComponent();
        }

        private void AddMember_Load(object sender, EventArgs e)
        {
            unloadFieldElements();

            // gets list of groups
            string[] Files = Directory.GetFiles(box.getDefaultDirectory(), "*.txt", SearchOption.AllDirectories); //Getting Text files
            
            foreach (string element in Files)
            {
                string newElement = Path.GetFileName(element);
                newElement = newElement.Substring(0, newElement.Length - 4);
                if (newElement.Contains("settings"))
                {

                }
                else
                {
                    if (newElement.Contains("_group"))
                    {
                        newElement = newElement.Substring(0, newElement.Length - 6);
                        groupsComboBox.Items.Add(newElement);
                    }
                }
            }
        }

        private void unloadFieldElements()
        {
            // makes fields and folders elements invisible
            field1Label.Visible = false;
            field2Label.Visible = false;
            field3Label.Visible = false;
            field4Label.Visible = false;

            field1TextBox.Visible = false;
            field2TextBox.Visible = false;
            field3TextBox.Visible = false;
            field4TextBox.Visible = false;

            folderComboBox.Items.Clear();
        }

        private void groupsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            unloadFieldElements();

            string[] Files = Directory.GetFiles(box.getDefaultDirectory(), "*.txt", SearchOption.AllDirectories); //Getting Text files
            string selectedItem = groupsComboBox.SelectedItem.ToString();
            if (Files.Contains(System.IO.Path.Combine(box.getDefaultDirectory(), selectedItem + "_group.txt")))
            {
                


                // Reads the selected file, then gets line 1 and 2
                System.IO.StreamReader settingsFile =
                   new System.IO.StreamReader(System.IO.Path.Combine(
                       box.getDefaultDirectory(),selectedItem + "_group_settings.txt"));

                // reads the first two lines
                string line1 = settingsFile.ReadLine();
                string line2 = settingsFile.ReadLine();

                settingsFile.Close();

                // splits line 1 and line 2
                string[] line1Split = line1.Split('~');
                string[] line2Split = line2.Split('~');

                string[] folders = line2Split[0].Split(',');


                LaDa groupTable = new LaDa(selectedItem + "_group");
                List<string> headersList = groupTable.getHeaders();

                headersList = headersList.Skip(2).ToArray().ToList();

                // adds the folder names to the folder combo box
                foreach (string element in folders)
                {
                    folderComboBox.Items.Add(element);
                }

                // makes folder combo box visible
                folderComboBox.Visible = true;

                // makes all other elements visible
                int amountOfFields = headersList.Count;

                if (amountOfFields == 4)
                {
                    field1Label.Text = headersList[0];
                    field2Label.Text = headersList[1];
                    field3Label.Text = headersList[2];
                    field4Label.Text = headersList[3];

                    field1Label.Visible = true;
                    field2Label.Visible = true;
                    field3Label.Visible = true;
                    field4Label.Visible = true;

                    field1TextBox.Visible = true;
                    field2TextBox.Visible = true;
                    field3TextBox.Visible = true;
                    field4TextBox.Visible = true;
                }
                else if (amountOfFields == 3)
                {
                    field1Label.Text = headersList[0];
                    field2Label.Text = headersList[1];
                    field3Label.Text = headersList[2];

                    field1Label.Visible = true;
                    field2Label.Visible = true;
                    field3Label.Visible = true;

                    field1TextBox.Visible = true;
                    field2TextBox.Visible = true;
                    field3TextBox.Visible = true;
                }
                else if (amountOfFields == 2)
                {
                    field1Label.Text = headersList[0];
                    field2Label.Text = headersList[1];

                    field1Label.Visible = true;
                    field2Label.Visible = true;

                    field1TextBox.Visible = true;
                    field2TextBox.Visible = true;
                }
                else
                {
                    field1Label.Text = headersList[0];

                    field1Label.Visible = true;

                    field1TextBox.Visible = true;
                }
            }
            else
            {
                // throw error to select proper group
            }
        }

        private void addMemberButton_Click(object sender, EventArgs e)
        {
            List<string> fields = new List<string>();
            if (groupsComboBox.SelectedItem.ToString() == "")
            {
                // throws error
                return;
            }
            if (folderComboBox.SelectedItem.ToString() == "")
            {
                // throws error
                return;
            }
            if (selectFileTextBox.Text == "")
            {
                // throws error
                return;
            }
            // checks to make sure all is inputted
            if (field4TextBox.Visible == false)
            {
               if (field3TextBox.Text != "" && field2TextBox.Text != "" && field1TextBox.Text != "")
                {
                    fields.Add(field1TextBox.Text);
                    fields.Add(field2TextBox.Text);
                    fields.Add(field3TextBox.Text);
                }
            }
            else if (field3TextBox.Visible == false)
            {
                if (field2TextBox.Text != "" && field1TextBox.Text != "")
                {
                    fields.Add(field1TextBox.Text);
                    fields.Add(field2TextBox.Text);
                }
            }
            else if (field2TextBox.Visible == false)
            {
                if (field1TextBox.Text != "")
                {
                    fields.Add(field1TextBox.Text);
                }
            }
            else if (field1TextBox.Visible == false)
            {
                // throws error
                return;
            }
            // runs if all fields are filled
            else
            {
                if (field4TextBox.Text != "" && field3TextBox.Text != "" && field2TextBox.Text != "" && field1TextBox.Text != "")
                {
                    fields.Add(field1TextBox.Text);
                    fields.Add(field2TextBox.Text);
                    fields.Add(field3TextBox.Text);
                    fields.Add(field4TextBox.Text);
                }
            }

            string selectedItem = groupsComboBox.SelectedItem.ToString();

            /* legacy code

            // Reads the selected file, then gets line 1 and 2
            System.IO.StreamReader file =
               new System.IO.StreamReader(System.IO.Path.Combine(
                   box.getDefaultDirectory(), selectedItem + "_group.txt"));

            string line1 = file.ReadLine();
            string line2 = file.ReadLine();

            // closes the file stream
            file.Close();

            // gets the fields and folders into an array
            string[] fieldsArray = line1.Split('~');
            string[] foldersArray = line2.Split('~');
            fieldsArray = fieldsArray.Skip(1).ToArray();

            // gets the types
            System.IO.StreamReader settings =
               new System.IO.StreamReader(System.IO.Path.Combine(
                   box.getDefaultDirectory(), selectedItem + "_group_settings.txt"));

            line1 = settings.ReadLine();
            line2 = settings.ReadLine();
            List<string> typesList = new List<string>();
            string[] types = line2.Split('~');

            for (int i = 0; i < types.Length - 1; i++)
            {
                typesList.Add(types[i]);
            }

            settings.Close();

            */

            // finally adds the member to the group/file
            Group group = new Group(selectedItem);
            group.addMember(selectFileTextBox.Text, folderComboBox.SelectedItem.ToString(), fields);

            this.Close();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void selectFileButton_Click(object sender, EventArgs e)
        {
            if (selectFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                selectFileTextBox.Text = selectFileDialog.FileName;
            }
        }
    }
}
