﻿namespace DoUp
{
    partial class CreateGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.field1TextBox = new System.Windows.Forms.TextBox();
            this.field1TextRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.field1NumRadio = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.field2NumRadio = new System.Windows.Forms.RadioButton();
            this.field2TextRadio = new System.Windows.Forms.RadioButton();
            this.field2TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.field3NumRadio = new System.Windows.Forms.RadioButton();
            this.field3TextRadio = new System.Windows.Forms.RadioButton();
            this.field3TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.field4NumRadio = new System.Windows.Forms.RadioButton();
            this.field4TextRadio = new System.Windows.Forms.RadioButton();
            this.field4TextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupNameTextBox = new System.Windows.Forms.TextBox();
            this.foldersTextBox = new System.Windows.Forms.TextBox();
            this.createGroupButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Folders:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Field 1:";
            // 
            // field1TextBox
            // 
            this.field1TextBox.Location = new System.Drawing.Point(102, 182);
            this.field1TextBox.Name = "field1TextBox";
            this.field1TextBox.Size = new System.Drawing.Size(214, 31);
            this.field1TextBox.TabIndex = 3;
            // 
            // field1TextRadio
            // 
            this.field1TextRadio.AutoSize = true;
            this.field1TextRadio.Location = new System.Drawing.Point(6, 30);
            this.field1TextRadio.Name = "field1TextRadio";
            this.field1TextRadio.Size = new System.Drawing.Size(85, 29);
            this.field1TextRadio.TabIndex = 4;
            this.field1TextRadio.TabStop = true;
            this.field1TextRadio.Text = "Text";
            this.field1TextRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.field1NumRadio);
            this.groupBox1.Controls.Add(this.field1TextRadio);
            this.groupBox1.Location = new System.Drawing.Point(388, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 70);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // field1NumRadio
            // 
            this.field1NumRadio.AutoSize = true;
            this.field1NumRadio.Location = new System.Drawing.Point(165, 30);
            this.field1NumRadio.Name = "field1NumRadio";
            this.field1NumRadio.Size = new System.Drawing.Size(118, 29);
            this.field1NumRadio.TabIndex = 5;
            this.field1NumRadio.TabStop = true;
            this.field1NumRadio.Text = "Number";
            this.field1NumRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.field2NumRadio);
            this.groupBox2.Controls.Add(this.field2TextRadio);
            this.groupBox2.Location = new System.Drawing.Point(388, 257);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(299, 70);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // field2NumRadio
            // 
            this.field2NumRadio.AutoSize = true;
            this.field2NumRadio.Location = new System.Drawing.Point(165, 30);
            this.field2NumRadio.Name = "field2NumRadio";
            this.field2NumRadio.Size = new System.Drawing.Size(118, 29);
            this.field2NumRadio.TabIndex = 5;
            this.field2NumRadio.TabStop = true;
            this.field2NumRadio.Text = "Number";
            this.field2NumRadio.UseVisualStyleBackColor = true;
            // 
            // field2TextRadio
            // 
            this.field2TextRadio.AutoSize = true;
            this.field2TextRadio.Location = new System.Drawing.Point(6, 30);
            this.field2TextRadio.Name = "field2TextRadio";
            this.field2TextRadio.Size = new System.Drawing.Size(85, 29);
            this.field2TextRadio.TabIndex = 4;
            this.field2TextRadio.TabStop = true;
            this.field2TextRadio.Text = "Text";
            this.field2TextRadio.UseVisualStyleBackColor = true;
            // 
            // field2TextBox
            // 
            this.field2TextBox.Location = new System.Drawing.Point(102, 283);
            this.field2TextBox.Name = "field2TextBox";
            this.field2TextBox.Size = new System.Drawing.Size(214, 31);
            this.field2TextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 286);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Field 2:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.field3NumRadio);
            this.groupBox3.Controls.Add(this.field3TextRadio);
            this.groupBox3.Location = new System.Drawing.Point(394, 363);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 70);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // field3NumRadio
            // 
            this.field3NumRadio.AutoSize = true;
            this.field3NumRadio.Location = new System.Drawing.Point(165, 30);
            this.field3NumRadio.Name = "field3NumRadio";
            this.field3NumRadio.Size = new System.Drawing.Size(118, 29);
            this.field3NumRadio.TabIndex = 5;
            this.field3NumRadio.TabStop = true;
            this.field3NumRadio.Text = "Number";
            this.field3NumRadio.UseVisualStyleBackColor = true;
            // 
            // field3TextRadio
            // 
            this.field3TextRadio.AutoSize = true;
            this.field3TextRadio.Location = new System.Drawing.Point(6, 30);
            this.field3TextRadio.Name = "field3TextRadio";
            this.field3TextRadio.Size = new System.Drawing.Size(85, 29);
            this.field3TextRadio.TabIndex = 4;
            this.field3TextRadio.TabStop = true;
            this.field3TextRadio.Text = "Text";
            this.field3TextRadio.UseVisualStyleBackColor = true;
            // 
            // field3TextBox
            // 
            this.field3TextBox.Location = new System.Drawing.Point(108, 389);
            this.field3TextBox.Name = "field3TextBox";
            this.field3TextBox.Size = new System.Drawing.Size(214, 31);
            this.field3TextBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Field 3:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.field4NumRadio);
            this.groupBox4.Controls.Add(this.field4TextRadio);
            this.groupBox4.Location = new System.Drawing.Point(394, 465);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(299, 70);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            // 
            // field4NumRadio
            // 
            this.field4NumRadio.AutoSize = true;
            this.field4NumRadio.Location = new System.Drawing.Point(165, 30);
            this.field4NumRadio.Name = "field4NumRadio";
            this.field4NumRadio.Size = new System.Drawing.Size(118, 29);
            this.field4NumRadio.TabIndex = 5;
            this.field4NumRadio.TabStop = true;
            this.field4NumRadio.Text = "Number";
            this.field4NumRadio.UseVisualStyleBackColor = true;
            // 
            // field4TextRadio
            // 
            this.field4TextRadio.AutoSize = true;
            this.field4TextRadio.Location = new System.Drawing.Point(6, 30);
            this.field4TextRadio.Name = "field4TextRadio";
            this.field4TextRadio.Size = new System.Drawing.Size(85, 29);
            this.field4TextRadio.TabIndex = 4;
            this.field4TextRadio.TabStop = true;
            this.field4TextRadio.Text = "Text";
            this.field4TextRadio.UseVisualStyleBackColor = true;
            // 
            // field4TextBox
            // 
            this.field4TextBox.Location = new System.Drawing.Point(108, 491);
            this.field4TextBox.Name = "field4TextBox";
            this.field4TextBox.Size = new System.Drawing.Size(214, 31);
            this.field4TextBox.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 494);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Field 4:";
            // 
            // groupNameTextBox
            // 
            this.groupNameTextBox.Location = new System.Drawing.Point(180, 26);
            this.groupNameTextBox.Name = "groupNameTextBox";
            this.groupNameTextBox.Size = new System.Drawing.Size(211, 31);
            this.groupNameTextBox.TabIndex = 15;
            // 
            // foldersTextBox
            // 
            this.foldersTextBox.Location = new System.Drawing.Point(132, 100);
            this.foldersTextBox.Name = "foldersTextBox";
            this.foldersTextBox.Size = new System.Drawing.Size(211, 31);
            this.foldersTextBox.TabIndex = 16;
            // 
            // createGroupButton
            // 
            this.createGroupButton.Location = new System.Drawing.Point(559, 564);
            this.createGroupButton.Name = "createGroupButton";
            this.createGroupButton.Size = new System.Drawing.Size(177, 89);
            this.createGroupButton.TabIndex = 17;
            this.createGroupButton.Text = "Create Group";
            this.createGroupButton.UseVisualStyleBackColor = true;
            this.createGroupButton.Click += new System.EventHandler(this.createGroupButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(371, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(243, 25);
            this.label7.TabIndex = 18;
            this.label7.Text = "* Separate with commas";
            // 
            // CreateGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 665);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.createGroupButton);
            this.Controls.Add(this.foldersTextBox);
            this.Controls.Add(this.groupNameTextBox);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.field4TextBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.field3TextBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.field2TextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.field1TextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CreateGroup";
            this.Text = "Create Group";
            this.Load += new System.EventHandler(this.CreateGroup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox field1TextBox;
        private System.Windows.Forms.RadioButton field1TextRadio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton field1NumRadio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton field2NumRadio;
        private System.Windows.Forms.RadioButton field2TextRadio;
        private System.Windows.Forms.TextBox field2TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton field3NumRadio;
        private System.Windows.Forms.RadioButton field3TextRadio;
        private System.Windows.Forms.TextBox field3TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton field4NumRadio;
        private System.Windows.Forms.RadioButton field4TextRadio;
        private System.Windows.Forms.TextBox field4TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox groupNameTextBox;
        private System.Windows.Forms.TextBox foldersTextBox;
        private System.Windows.Forms.Button createGroupButton;
        private System.Windows.Forms.Label label7;
    }
}