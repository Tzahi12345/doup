﻿namespace DoUp
{
    partial class AddMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupsComboBox = new System.Windows.Forms.ComboBox();
            this.field4TextBox = new System.Windows.Forms.TextBox();
            this.field4Label = new System.Windows.Forms.Label();
            this.field3TextBox = new System.Windows.Forms.TextBox();
            this.field3Label = new System.Windows.Forms.Label();
            this.field2TextBox = new System.Windows.Forms.TextBox();
            this.field2Label = new System.Windows.Forms.Label();
            this.field1TextBox = new System.Windows.Forms.TextBox();
            this.field1Label = new System.Windows.Forms.Label();
            this.selectFolderLabel = new System.Windows.Forms.Label();
            this.folderComboBox = new System.Windows.Forms.ComboBox();
            this.addMemberButton = new System.Windows.Forms.Button();
            this.selectFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.selectFileButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.selectFileTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Group:";
            // 
            // groupsComboBox
            // 
            this.groupsComboBox.FormattingEnabled = true;
            this.groupsComboBox.Location = new System.Drawing.Point(179, 10);
            this.groupsComboBox.Name = "groupsComboBox";
            this.groupsComboBox.Size = new System.Drawing.Size(276, 33);
            this.groupsComboBox.TabIndex = 2;
            this.groupsComboBox.SelectedIndexChanged += new System.EventHandler(this.groupsComboBox_SelectedIndexChanged);
            // 
            // field4TextBox
            // 
            this.field4TextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.field4TextBox.Location = new System.Drawing.Point(404, 433);
            this.field4TextBox.Name = "field4TextBox";
            this.field4TextBox.Size = new System.Drawing.Size(385, 31);
            this.field4TextBox.TabIndex = 21;
            // 
            // field4Label
            // 
            this.field4Label.AutoSize = true;
            this.field4Label.Location = new System.Drawing.Point(12, 436);
            this.field4Label.Name = "field4Label";
            this.field4Label.Size = new System.Drawing.Size(83, 25);
            this.field4Label.TabIndex = 20;
            this.field4Label.Text = "Field 4:";
            // 
            // field3TextBox
            // 
            this.field3TextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.field3TextBox.Location = new System.Drawing.Point(404, 338);
            this.field3TextBox.Name = "field3TextBox";
            this.field3TextBox.Size = new System.Drawing.Size(376, 31);
            this.field3TextBox.TabIndex = 19;
            // 
            // field3Label
            // 
            this.field3Label.AutoSize = true;
            this.field3Label.Location = new System.Drawing.Point(12, 341);
            this.field3Label.Name = "field3Label";
            this.field3Label.Size = new System.Drawing.Size(83, 25);
            this.field3Label.TabIndex = 18;
            this.field3Label.Text = "Field 3:";
            // 
            // field2TextBox
            // 
            this.field2TextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.field2TextBox.Location = new System.Drawing.Point(404, 246);
            this.field2TextBox.Name = "field2TextBox";
            this.field2TextBox.Size = new System.Drawing.Size(376, 31);
            this.field2TextBox.TabIndex = 17;
            // 
            // field2Label
            // 
            this.field2Label.AutoSize = true;
            this.field2Label.Location = new System.Drawing.Point(12, 249);
            this.field2Label.Name = "field2Label";
            this.field2Label.Size = new System.Drawing.Size(83, 25);
            this.field2Label.TabIndex = 16;
            this.field2Label.Text = "Field 2:";
            // 
            // field1TextBox
            // 
            this.field1TextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.field1TextBox.Location = new System.Drawing.Point(404, 153);
            this.field1TextBox.Name = "field1TextBox";
            this.field1TextBox.Size = new System.Drawing.Size(376, 31);
            this.field1TextBox.TabIndex = 15;
            // 
            // field1Label
            // 
            this.field1Label.AutoSize = true;
            this.field1Label.Location = new System.Drawing.Point(12, 156);
            this.field1Label.Name = "field1Label";
            this.field1Label.Size = new System.Drawing.Size(83, 25);
            this.field1Label.TabIndex = 14;
            this.field1Label.Text = "Field 1:";
            // 
            // selectFolderLabel
            // 
            this.selectFolderLabel.AutoSize = true;
            this.selectFolderLabel.Location = new System.Drawing.Point(13, 78);
            this.selectFolderLabel.Name = "selectFolderLabel";
            this.selectFolderLabel.Size = new System.Drawing.Size(145, 25);
            this.selectFolderLabel.TabIndex = 22;
            this.selectFolderLabel.Text = "Select Folder:";
            // 
            // folderComboBox
            // 
            this.folderComboBox.FormattingEnabled = true;
            this.folderComboBox.Location = new System.Drawing.Point(179, 75);
            this.folderComboBox.Name = "folderComboBox";
            this.folderComboBox.Size = new System.Drawing.Size(276, 33);
            this.folderComboBox.TabIndex = 23;
            // 
            // addMemberButton
            // 
            this.addMemberButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addMemberButton.Location = new System.Drawing.Point(629, 660);
            this.addMemberButton.Name = "addMemberButton";
            this.addMemberButton.Size = new System.Drawing.Size(180, 86);
            this.addMemberButton.TabIndex = 24;
            this.addMemberButton.Text = "Add Member";
            this.addMemberButton.UseVisualStyleBackColor = true;
            this.addMemberButton.Click += new System.EventHandler(this.addMemberButton_Click);
            // 
            // selectFileDialog
            // 
            this.selectFileDialog.FileName = "selectFIleDialog";
            this.selectFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // selectFileButton
            // 
            this.selectFileButton.Location = new System.Drawing.Point(404, 536);
            this.selectFileButton.Name = "selectFileButton";
            this.selectFileButton.Size = new System.Drawing.Size(49, 33);
            this.selectFileButton.TabIndex = 25;
            this.selectFileButton.Text = "...";
            this.selectFileButton.UseVisualStyleBackColor = true;
            this.selectFileButton.Click += new System.EventHandler(this.selectFileButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 540);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 25);
            this.label2.TabIndex = 26;
            this.label2.Text = "Select File:";
            // 
            // selectFileTextBox
            // 
            this.selectFileTextBox.Location = new System.Drawing.Point(139, 536);
            this.selectFileTextBox.Name = "selectFileTextBox";
            this.selectFileTextBox.ReadOnly = true;
            this.selectFileTextBox.Size = new System.Drawing.Size(245, 31);
            this.selectFileTextBox.TabIndex = 27;
            // 
            // AddMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 758);
            this.Controls.Add(this.selectFileTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.selectFileButton);
            this.Controls.Add(this.addMemberButton);
            this.Controls.Add(this.folderComboBox);
            this.Controls.Add(this.selectFolderLabel);
            this.Controls.Add(this.field4TextBox);
            this.Controls.Add(this.field4Label);
            this.Controls.Add(this.field3TextBox);
            this.Controls.Add(this.field3Label);
            this.Controls.Add(this.field2TextBox);
            this.Controls.Add(this.field2Label);
            this.Controls.Add(this.field1TextBox);
            this.Controls.Add(this.field1Label);
            this.Controls.Add(this.groupsComboBox);
            this.Controls.Add(this.label1);
            this.Name = "AddMember";
            this.Text = "Add Member";
            this.Load += new System.EventHandler(this.AddMember_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox groupsComboBox;
        private System.Windows.Forms.TextBox field4TextBox;
        private System.Windows.Forms.Label field4Label;
        private System.Windows.Forms.TextBox field3TextBox;
        private System.Windows.Forms.Label field3Label;
        private System.Windows.Forms.TextBox field2TextBox;
        private System.Windows.Forms.Label field2Label;
        private System.Windows.Forms.TextBox field1TextBox;
        private System.Windows.Forms.Label field1Label;
        private System.Windows.Forms.Label selectFolderLabel;
        private System.Windows.Forms.ComboBox folderComboBox;
        private System.Windows.Forms.Button addMemberButton;
        private System.Windows.Forms.OpenFileDialog selectFileDialog;
        private System.Windows.Forms.Button selectFileButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox selectFileTextBox;
    }
}