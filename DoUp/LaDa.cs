﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoUp
{
    // LaDa stands for Local Data, a local alternative to SQL. There are currently some methods in here that are extraneous
    class LaDa
    {
        Toolbox box = new Toolbox();

        // instance variable
        public string tableName;

        // constructor for LaDa object
        public LaDa(string name)
        {
            tableName = name;
        }

        // creates file named after the table
        public void createTable(List<string> headers)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string firstLine = "";
            if (headers.Count > 0)
            {
                firstLine = headers[0];
            }
            for (int i = 1; i < headers.Count; i++)
            {
                firstLine += "~" + headers[i];
            }
            // checks if file exists
            if (File.Exists(tableFileName))
            {
                //return error
            }
            else
            {
                // writes headers to file
                using (StreamWriter sw = File.CreateText(tableFileName))
                {
                    sw.WriteLine(firstLine);
                }

            }
        }

        // adds a row to a table
        public void addRow(List<string> values)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string newLine = "";
            if (values.Count > 0)
            {
                newLine = values[0];
            }
            // defines first line of the text file
            for (int i = 1; i < values.Count; i++)
            {
                newLine += "~" + values[i];
            }
            if (!(File.Exists(tableFileName)))
            {
                //return error
            }
            else
            {
                // writes new data to file
                using (StreamWriter sw = File.AppendText(tableFileName))
                {
                    sw.WriteLine(newLine);
                }
            }
        }

        // gets value of a particular row at a particular header
        public string getValue(string identifierHeader, string identifierValue, string valueHeader)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string line;
            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // gets headers in file
            line = file.ReadLine();
            // tries to locate location of header
            string[] headers = line.Split('~');
            int indexOfIdentifierHeader = Array.IndexOf(headers,identifierHeader);
            int indexOfValueHeader = Array.IndexOf(headers, valueHeader);

            while ((line = file.ReadLine()) != null)
            {
                string[] values = line.Split('~');
                bool identifierFound = values[indexOfIdentifierHeader] == identifierValue;
                if (identifierFound)
                {
                    return values[indexOfValueHeader];
                }
            }
            file.Close();
            return "";
        }

        // gets the headers in a table and returns it as a string
        public List<string> getHeaders()
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string line;
            // reads the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // gets headers in file
            line = file.ReadLine();

            // splits headers
            string[] headers = line.Split('~');

            List<string> headersList = headers.ToList();
            file.Close();
            return headersList;
        }

        // checks if the table exists
        public bool existsTable(string tableName)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string groupFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            if (!(File.Exists(groupFileName)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // checks if a value exists in a specific header of a table
        public bool existsValue(string header, string value)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string line;
            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // gets headers in file
            line = file.ReadLine();

            // Closes the file
            file.Close();

            // tries to locate location of header
            string[] headers = line.Split('~');

            int indexOfValueHeader = Array.IndexOf(headers, header);

            while ((line = file.ReadLine()) != null)
            {
                string[] values = line.Split('~');
                bool identifierFound = values[indexOfValueHeader] == value;
                if (identifierFound)
                {
                    return true;
                }
            }
            
            return false;
        }

        // gets amount of headers in a table
        public int headersCount()
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string line;
            // reads the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // gets headers in file
            line = file.ReadLine();

            // tries to locate location of header
            string[] headers = line.Split('~');
            file.Close();
            return headers.Length;
        }

        // changes the value of a cell
        public void changeValue(string identifierHeader, string identifierValue, string valueHeader, string newValue)
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string tempFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + "_temp.txt");
            string line;
            // read the file and display it line by line
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // writes data to temporary file
            using (StreamWriter sw = File.CreateText(tempFileName))
            {
                // gets headers in file then writes line to temp text file
                line = file.ReadLine();
                sw.WriteLine(line);

                // tries to locate location of header
                string[] headers = line.Split('~');
                int indexOfIdentifierHeader = Array.IndexOf(headers, identifierHeader);
                int indexOfValueHeader = Array.IndexOf(headers, valueHeader);

                while ((line = file.ReadLine()) != null)
                {
                    string[] values = line.Split('~');
                    bool identifierFound = values[indexOfIdentifierHeader] == identifierValue;
                    if (identifierFound)
                    {
                        values[indexOfValueHeader] = newValue;
                        string newLine = "";
                        if (values.Length > 0)
                        {
                            newLine = values[0];
                        }
                        for (int i = 1; i < values.Length; i++)
                        {
                            newLine += "~" + values[i];
                        }
                        sw.WriteLine(newLine);
                    }
                    else
                    {
                        sw.WriteLine(line);
                    }
                }
            }
            file.Close();
            // copies temporary file over to original file
            File.Delete(tableFileName);
            File.Copy(tempFileName, tableFileName);
            // deletes temporary file
            File.Delete(tempFileName);
        }

        // adds a header to a table, and replaces the missing cells with defaultValue
        public void addHeader(string newHeaderName, string defaultValue, int location = -1)
        {
            int headersAmount = headersCount();

            // no input in location and any location above the header count is simply appended
            if (location == -1 || location > headersAmount + 1)
            {
                location = headersAmount + 1;
            }

            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");
            string tempFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + "_temp.txt");
            string line;
            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);

            // writes data to temporary file
            using (StreamWriter sw = File.CreateText(tempFileName))
            {
                // gets headers in file then writes line to temp text file
                line = file.ReadLine();
                sw.WriteLine(line);

                // tries to locate location of header
                string[] headers = line.Split('~');

                string newHeaders = insertItem(headers, location, newHeaderName);

                // writes out the new headers
                sw.WriteLine(newHeaders);

                while ((line = file.ReadLine()) != null)
                {
                    string[] values = line.Split('~');

                    string newValues = insertItem(values, location, defaultValue);

                    sw.WriteLine(newValues);
                    
                }
            }
            file.Close();

            // copies temporary file over to original file
            File.Delete(tableFileName);
            File.Copy(tempFileName, tableFileName);
            // deletes temporary file
            File.Delete(tempFileName);
        }

        // methods for specific tasks

        // inserts item into array delimited by ~
        public string insertItem(string[] arr, int location, string item)
        {
            bool toAppend;
            if (location > arr.Length + 1)
            {
                toAppend = true;
            }
            else
            {
                toAppend = false;
            }
            string result = "";
            if (arr.Length > 0)
            {
                result = arr[0];
            }

            if (toAppend)
            {
                for (int i = 1; i < arr.Length; i++)
                {
                    result += "~" + arr[i];
                }
                result += "~" + item;
            }
            else if (location == 1)
            {
                for (int i = 1; i < arr.Length; i++)
                {
                    result += "~" + arr[i];
                }
                result = item + "~" + result;
            }
            else
            {
                for (int i = 1; i < arr.Length; i++)
                {
                    if (i == location - 1)
                    {
                        result += "~" + result;
                    }
                    result += "~" + arr[i];
                }
            }
            return result;
        }

        // converts table to 2D Array
        public string[][] to2DArray()
        {
            string defaultDirectory = box.getDefaultDirectory();
            string tableFileName = System.IO.Path.Combine(
                defaultDirectory, tableName + ".txt");

            // gets number of rows
            System.IO.StreamReader temp =
                new System.IO.StreamReader(tableFileName);
            int counter = 0;
            string line;
            while ((line = temp.ReadLine()) != null)
            {
                counter++;
            }
                temp.Close();
            int rows = counter;
            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(tableFileName);
            line = file.ReadLine();
            string[] headers = line.Split('~');
            int cols = headers.Length;
            string[][] twoDArr = new string[rows][];

            counter = 0;
            while ((line = file.ReadLine()) != null)
            {
                string[] values = line.Split('~');
                for (int i = 0; i < cols; i++)
                {
                    twoDArr[counter][i] = values[i];
                }
                counter++;
            }
            file.Close();
            return twoDArr;
        }
    }
    // class with tools relating to encryption
    class LaEn
    {

    }
}
