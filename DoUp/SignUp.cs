﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoUp
{
    public partial class SignUp : Form
    {
        LaDa lada = new LaDa("users");
        public SignUp()
        {
            InitializeComponent();
        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            string username = usernameTextBox.Text;

            // checks to make sure there are no problems in making user
            if (usernameTextBox.Text.Length < 5) 
            {
                MessageBox.Show("Username too short.");
            }
            else if (passwordTextBox.Text.Length < 6)
            {
                MessageBox.Show("Password too short.");
            }
            else if (passwordTextBox.Text != passwordConfirmTextBox.Text)
            {
                MessageBox.Show("Passwords do not match.");
            }
            // runs if no problems in username and password
            else
            {
                if (lada.existsTable("users"))
                {
                    if (lada.existsValue("username",username))
                    {
                        MessageBox.Show("Username already exists.");
                    }
                    else
                    {
                        List<string> headers = new List<string>();
                        headers.Add(usernameTextBox.Text);
                        headers.Add(passwordTextBox.Text);
                        lada.addRow(headers);
                        MessageBox.Show("You have successfully signed up!");
                        this.Close();
                    }
                }
                else
                {
                    List<string> tableHeaders = new List<string>();
                    tableHeaders.Add("username");
                    tableHeaders.Add("password");
                    lada.createTable(tableHeaders);

                    List<string> headers = new List<string>();
                    headers.Add(usernameTextBox.Text);
                    headers.Add(passwordTextBox.Text);
                    lada.addRow(headers);
                    MessageBox.Show("You have successfully signed up!");
                    this.Close();
                }
            }              
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }
    }
}
